CREATE OR REPLACE FUNCTION select_proc2(int)
  RETURNS SETOF unknown AS
$func$
SELECT 'name' FROM organization where id = $1;
$func$ LANGUAGE sql