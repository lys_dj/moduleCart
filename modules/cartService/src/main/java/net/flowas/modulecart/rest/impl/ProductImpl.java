package net.flowas.modulecart.rest.impl;

import java.io.InputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import net.flowas.modulecart.domain.Product;
import net.flowas.modulecart.rest.ProductEndpoint;

/**
 * 
 */
public class ProductImpl extends AbstractEndpoint<Product> implements ProductEndpoint {
	private EntityManager em = Persistence.createEntityManagerFactory("cartPU")
			.createEntityManager();

	// @Produces("application/octet-stream")
	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.ProductEndpoint#images(java.lang.Long)
	 */
	@Override
	public byte[] images(Long id) throws Exception {
		System.out.println(id);
		InputStream fi = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream("META-INF/resources/" + id + ".jpg");
		byte[] b = new byte[fi.available()];
		fi.read(b);
		fi.close();
		return b;
	}

	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.ProductEndpoint#findById(java.lang.Long)
	 */
	@Override
	public Product findById(Long id) {
		TypedQuery<Product> findByIdQuery = em
				.createQuery(
						"SELECT DISTINCT c FROM Product c LEFT JOIN FETCH c.parent LEFT JOIN FETCH c.children WHERE c.id = :entityId ORDER BY c.id",
						Product.class);
		findByIdQuery.setParameter("entityId", id);
		Product entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			//return Response.status(Status.NOT_FOUND).build();
		}
		return entity;
	}

	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.ProductEndpoint#listAll(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Product> listAll(Integer startPosition,
			Integer maxResult) {
		TypedQuery<Product> findAllQuery = em.createQuery(
				"SELECT DISTINCT c FROM Product c ORDER BY c.code",
				Product.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Product> results = findAllQuery.getResultList();
		return results;
	}

	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.ProductEndpoint#listLast(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Product> listLast(Integer startPosition,
			Integer maxResult) {
		TypedQuery<Product> findAllQuery = em.createQuery(
				"SELECT DISTINCT c FROM Product c ORDER BY c.code",
				Product.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Product> results = findAllQuery.getResultList();
		return results;
	}

	/* (non-Javadoc)
	 * @see net.flowas.modulecart.rest.impl.ProductEndpoint#listPromos(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<Product> listPromos(Integer startPosition,
			Integer maxResult) {
		TypedQuery<Product> findAllQuery = em.createQuery(
				"SELECT DISTINCT c FROM Product c ORDER BY c.code",
				Product.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Product> results = findAllQuery.getResultList();
		return results;
	}

	@Override
	protected Class<Product> getType() {
		return Product.class;
	}
}