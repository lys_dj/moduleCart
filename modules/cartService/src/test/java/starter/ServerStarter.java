package starter;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.resource.ResourceCollection;
import org.eclipse.jetty.webapp.WebAppContext;


public class ServerStarter {
	static int port = 9180;
	public static void main(String[] args) throws Exception {
		Server server = new Server(port);	    
    	WebAppContext war = new WebAppContext("src/test/resources/META-INF/webapp","/");
        war.setDescriptor("src/test/resources/META-INF/webapp/WEB-INF/web.xml");
        server.setHandler(war);
        server.start();    
        jarResource(war);
		Thread.currentThread().wait();
	}
    private static void jarResource(WebAppContext war) throws IOException{
    	//add jar resources
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resourceEnum = classLoader.getResources("META-INF/resources");
    	Set<Resource> resources=new HashSet<Resource>();
        while(resourceEnum.hasMoreElements()){
    		URL url=resourceEnum.nextElement();
    		//if(!url.toExternalForm().contains("jar!")){
    		//	continue;
    		//}
            resources.add(Resource.newResource(url));
    	}
        if (resources!=null)
        {           
        	Resource[] collection=new Resource[resources.size()+1];
            int i=0;
            collection[i++]=war.getBaseResource();
            for (Resource resource : resources)
                collection[i++]=resource;
            war.setBaseResource(new ResourceCollection(collection));
        }
    }
}
